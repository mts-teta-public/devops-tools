#### 
GUI клиент для kubernetes:
https://k8slens.dev/


Шаблон golang проекта:
https://gitlab.com/mts-teta-public/example-app


## 1. Общий шаблон Gitlab CI
Для сборки сервисов используется общий шаблон gitlab CI, который можно переиспользовать просто вставив в `.gitlab-ci.yml` файл своего репозитория следующие строки:
```yaml
include:
  remote: 'https://gitlab.com/mts-teta-public/devops-tools/-/raw/master/.gitlab-ci-main.yml'
```

Стадии общего gitlab ci:
- image build(kaniko) - сборка docker образа на основе Dockerfile из корня репозитория
- k8s deploy - генерация манифестов с помощью общего helm чарта 


## 2. Общий Dockerfile
Для сборки приложений и конечного docker image используется multistage build, стадии сборки:
1. Сборка бинарника go
2. Перекладывание бинарника из предыдущей стадии в облегченный образ alpine

Ссылка: https://gitlab.com/mts-teta-public/devops-tools/-/raw/master/Dockerfile

## 3. Общий helm template 
На стадии k8s deploy в шаблонном CI файле используется генерация манифестов на основе helm чарта:

По умолчанию для сервиса используются следующие порты:
```
    - port: 3000
      targetPort: 3000
      protocol: TCP
      name: http
    - port: 4000
      targetPort: 4000
      protocol: TCP
      name: grpc
    - port: 9000
      targetPort: 9000
      protocol: TCP
      name: metrics
```

Ссылка: https://gitlab.com/mts-teta-public/devops-tools/-/tree/master/helm-chart


## 4. Использование kubectl
Настройка  контекста доступа к кластеру:
1. Положить выданный kubeconfig по следующему пути: `~/.kube/config` - будет использоваться по умолчанию
2. Или сделать экспорт переменной для текущей сесии терминала: `export KUBECONFIG=./team0`

Команды управления:
1. Просмотр подов в неймспейсе: `kubectl get pods`
2. Просмотр сервисов в неймспейсе: `kubectl get svc`
3. Просмотр логов приложения: `kubectl logs -f {pod-name}`
4. Просмотр статуса пода: `kubectl describe {pod-name}`
5. Проброс портов приложения: `kubectl port-forward {pod-name} 3000:3000`
6. Просмотр манифестов деплоймента: `kubectl get deployment {service-name} -oyaml`

Инструкция по установке: https://kubernetes.io/docs/tasks/tools/#kubectl

Использование алиасов для удобства и автодополнения: https://github.com/ahmetb/kubectl-aliases

Вспомогательные инструменты: 
- https://k9scli.io/topics/install/

## Наблюдаемость
Jaeger(traces):
- Jaeger query ui: https://jaeger.k8s.golang-mts-teta.ru
- Jaeger collector: jaeger-instance-collector.observability:14250 https://www.jaegertracing.io/docs/1.34/deployment/#collector
- Jaeger agent: jaeger-instance-collector.observability:6831 https://www.jaegertracing.io/docs/1.34/deployment/#agent

Grafana(metrics):
- ui: https://grafana.k8s.golang-mts-teta.ru

Kibana(logs):
- ui: https://kibana.k8s.golang-mts-teta.ru

Sentry:
- ui: https://sentry.k8s.golang-mts-teta.ru


## Запуск тестов с помощью newman

В ci/cd пайплайне на стадии test запускается коллекция postman с набором тестов.

Для запуска нужно коллекции необходимо в настройках проекта добавить следующие переменные:

`TEST_COLLECTION_NAME` - имя коллекции по типу сервиса, пример значения: auth  (возможные значения: task, auth)

`SERVICE_API_ENDPOINT` - api url тестируемого сервиса, пример значения: my-auth-service:3000/auth/v1


Набор тестов можно посмотреть тут:
https://gitlab.com/mts-teta-public/devops-tools/-/tree/master/postman-collections


### Шаблон golang проекта

https://gitlab.com/mts-teta-public/example-app


### Полезные ссылки

Kubernetes для разработчиков: https://blog.gusev.tech/k8s-for-developers/

Подготовка к CKAD: https://github.com/bmuschko/ckad-crash-course
